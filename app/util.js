const abi = require('ethjs-abi');
import Mustache from 'mustache'

//function to generate signature strings from an ABI
const generateFunctionSignatures = (contractABI) => {
    let functions = []
    for(var entry in contractABI){
        if (contractABI[entry].type === "function") {
            var desc = ''
            var functionABI = contractABI[entry]
            desc += functionABI.constant ? "constant " : "";
            desc += functionABI.payable ? " payable " : "";
            desc += functionABI.name + "("
            for(var i in functionABI.inputs){
                desc += functionABI.inputs[i].type + " " + functionABI.inputs[i].name
                if(i != functionABI.inputs.length - 1) {
                    desc += ","
                }
            }
            desc += ")"
            functions.push(desc)
        }
    }
    return functions
}

const generateEventSignatures = (contractABI, asTopic=false) => {
    let events = []
    for(var entry in contractABI){
        if (contractABI[entry].type === "event") {
            var desc = ''
            var eventABI = contractABI[entry]
            desc += eventABI.name + "("
            for(var i in eventABI.inputs) {
                desc += eventABI.inputs[i].type + (eventABI.inputs[i].indexed ? " indexed " : "") + " " + eventABI.inputs[i].name
                if(i != eventABI.inputs.length - 1) {
                    desc += ", "
                }
            }
            desc += ")"
            events.push(desc)
        }
    }
    return events
}

// functoin to generate a description from the contractInformation that is stored in state (abi, address)
const generateContractDescriptions = (contractData) => {
    var contracts = {}
    for (var contractName of Object.keys(contractData)) {
        var abi = contractData[contractName].abi
        contracts[contractName] = {}
        contracts[contractName].functions = generateFunctionSignatures(abi);
        contracts[contractName].events = generateEventSignatures(abi);
        contracts[contractName].address = contractData[contractName].address
        contracts[contractName].abi = abi
    }
    return contracts
}

const checkEventParams = (contracts, contractName, eventName=false, filterPairs=false) => {
    //TODO see if arguments are valid (e.g addresses make sense)
    if (!contracts[contractName]) {
        return [false, "The contract '" + contractName +  "' you filtered does not exist"]
    }
    // let contractName = args[0]
    let contractABI = contracts[contractName].abi
    let params;


    // check if a specified event exists
    if (eventName) {
        var eventObj = contractABI.filter(entry => entry.name === eventName)[0]
        if (!eventObj) {
            return [false, "The event '" + eventName +  "' you filtered does not exist"]
        }
        // check if filters are specified
        if (filterPairs) {
            params = {}
            for (var keyValString of filterPairs) {
                let keyVal = keyValString.split(":")
                //check in abi if key is actually a param
                let idx = eventObj.inputs.findIndex(x => x.name === keyVal[0])
                if (idx === -1) {
                    return [false, "The argument " + keyVal[0] + " you filtered for does not exist."]
                }
                params[keyVal[0]] = keyVal[1]
            }
        }
    }
    return [true, {
        eventName: eventName || "All",
        filterParams: params,
    }]
}

const isAddress = function (address) {
    //check if the address begins with 0x; is 40c long; and contains only 0-9 and a-f
    if (!/^(0x)?[0-9a-f]{40}$/i.test(address)) {
        return false;
    }
    return true;
};

export default {
    generateFunctionSignatures,
    generateContractDescriptions,
    isAddress,
    checkEventParams,
}
