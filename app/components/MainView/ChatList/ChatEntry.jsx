import React from 'react';
import Moment from 'react-moment';
import ReactJson from 'react-json-view'
import ReactMarkdown from 'react-markdown'
import {CopyToClipboard} from 'react-copy-to-clipboard';

const help = (props) => (
    <div>
        {Object.entries(props.entry.text).map( (docTuple,i) =>
            <p key={i}>
                {docTuple[0]} : {docTuple[1]}
            </p>
        )}
    </div>
)

const roomInfo = (props)=>(
    <div>
        <span>You're in: "{props.entry.text.metadata.name}" :  </span>
        <span>{props.entry.text.metadata.description}</span>
        <div>
            <span> Pre-loaded contracts: </span>
            <ReactJson
                src={props.entry.text.contracts}
                collapsed={2}
                name={false}
                displayDataTypes={false}
                displayObjectSize={false}
            />
        </div>
    </div>
)

const notification = (props) => (
    <div>
        <span><i>{props.entry.text}</i></span>
    </div>
)

const message = (props) => {
    let address = props.entry.from
    let handle = props.getNicknameOrAddress(address) || address.substring(0,5) + "..." + address.substring(37)
    return (
        <div className="messageView">
        <div className="userInfo">
            <span className="chat-sender" title={address}>
                <CopyToClipboard text={address}>
                    <span><b>{handle}</b></span>
                </CopyToClipboard>
            </span>
            <span> (<Moment className="time-stamp" fromNow>{props.entry.time}</Moment>): </span>
        </div>
        <ReactMarkdown source={props.entry.text}/>
    </div>)
}

const whisper = (props) => {
    var MessageComponent = message
    return (
        <div className="whisperView" style={{backgroundColor:"#888888"}}>
            <MessageComponent
                entry={props.entry}
                getNicknameOrAddress={props.getNicknameOrAddress}
            />
        </div>
    )
}

const alert = (props) => (
    <div>
        <span style={{ textColor:'red' }}> { props.entry.text }</span>
    </div>
)

const txHash = (props) => {
    var link = props.network.etherscan + "tx/" + props.entry.text
    return (
        <div>
            <span>
                <i>Transaction executed! See <a href={link}>etherscan</a> for details. </i>
            </span>
        </div>
    )
}

const eventLogs = (props) => {
    let filters = props.entry.text.filter.join(', ')
    let text =  ("'" + props.entry.text.eventName + "'-" || "All") + " events fired from '" + props.entry.text.from + "' " + filters
    return (
        <div style={{
                border: "solid",
                backgroundColor: "#D8D8D8",
                padding: '5px'
        }}>
            <span> {text} </span>
            <div style={{
                maxHeight: '300px',
                overflow: "auto",
            }}>

                    <ReactJson
                        src={props.entry.text.logs}
                        collapsed={1}
                        name={false}
                        displayDataTypes={false}
                        displayObjectSize={false}
                    />
            </div>
        </div>
    )
}

const nicknames = (props) => {
    let nicknames = {}
    //Remove address to nickname mapping from object
    Object.keys(props.entry.text).forEach((key) => {
        !(key.substring(0,2) === '0x') && (nicknames[key] = props.entry.text[key]);
    })
    return (
        <div>
            <span> Nicknames: </span>
            <ReactJson
                src={nicknames}
                collapsed={1}
                name={false}
                displayDataTypes={false}
                displayObjectSize={false}
            />
        </div>
    )
}

export default {
    help,
    roomInfo,
    notification,
    message,
    alert,
    txHash,
    eventLogs,
    nicknames,
    whisper
}
